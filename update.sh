#!/bin/bash


# Used as part of the auto-update process (only for official Manifold feeders)
sudo apt-get update
sudo apt-get install -y --force-yes --only-upgrade feedertweeter
