# About
We created [Feeder Tweeter][1] as an experiment aimed at connecting an ordinary object with the Internet at large. The end result is an autonomous solar powered bird feeder that detects motion, snaps photos and uploads them to Twitter.  We've open sourced everything so if you'd like to build your then have at it.

The original [announcement][2] can be found here.

# Setup Instructions

###Overview
This code will do you no good unless you have completed step 1.  Step 1 is obviously to build yourself a feeder and do all the wiring.  There is a [fritzing][3] diagram named "feedertweeter.fzz" in the fritzing directory of this download to help you with the wiring.  When you are ready for the code carry on...

Place the Feeder Tweeter code on your Raspberry Pi at the following location:
/home/pi/feedertweeter

###Python Scripts
There are 3 main Python scripts that make up the code for a Feeder Tweeter.  This code was developed against the [Raspbian][4] distribution, specifically the "wheezy" version.  Other distributions and/or versions have not been tested so your mileage may vary.  Here is a brief description of each script:

1. **camera.py** - This script handles the feeder's motion detection and image capturing.
2. **feedertweeter.py** - This script watches for new bird pictures to be captured and then posts them to Twitter.
3. **reset_monitor.py** - This script supports the external reboot/shutdown button found on the exterior of the feeder's enclosure.

Make sure these 3 scripts are executable:
cd /home/pi/feedertweeter; chmod +x *.py

These scripts require the following Python packages and modules:

* python-dev
* python-setuptools 
* twython
* fsmonitor

Here are some install commands to help you get those installed on your Raspberry Pi:

* sudo apt-get install python-serial vim python-dev python-setuptools
* sudo easy_install twython
* We experienced some issues with older versions on fsmonitor and found it easiest to [download][5] the latest version manually and install it locally.  The working version we use is included in the "libs" folder already.  To build it locally simply do the following:

cd /home/pi/feedertweeter/libs/fsmonitor; sudo python setup.py install

###System Startup (Supervisor)
Start all three scripts on system startup using [supervisor][6].  Install with the following command:

* sudo apt-get install supervisor

The default supervisor config location is /etc/supervisor/supervisord.conf.  Edit this file and add the following 3 entries at the bottom of the file:

[program:feedertweeter_reset]
command=/home/pi/feedertweeter/reset_monitor.py

[program:feedertweeter_camera]
command=/home/pi/feedertweeter/camera.py

[program:feedertweeter_twitter]
command=/home/pi/feedertweeter/feedertweeter.py
environment=FEEDERTWEETER_APP_KEY='YOUR-APP-KEY',FEEDERTWEETER_APP_SECRET='YOUR-APP-SECRET',FEEDERTWEETER_OAUTH_TOKEN='YOUR-OAUTH-TOKEN',FEEDERTWEETER_OAUTH_TOKEN_SECRET='YOUR-OAUTH-TOKEN-SECRET'

####NOTE:
The last entry has a special variable called "environment".  This allows environment variables to be set and passed to the supervisord processes.  In order to interact with the Twitter API and post bird pictures the feedertweeter.py script needs your Twitter OAuth information.  You will need to create a new Twitter application to generate this information.  Go to [https://dev.twitter.com/apps][7] and click create new application.  Fill out a few fields and submit it.  The next page will give you the information needed for the "environment" variable of that 3rd supervisor entry.  Make sure that when you generate your application you give it read and write permissions or you will not be able to create posts.

After a reboot all three scripts should be running.  You can test this with the following command:

ps -ef | grep feedertweeter

###Troubleshooting
If you do not see all three Python scripts listed in the output then Supervisor probably had an issues starting one or more of them.  The Supervisor log file (/var/log/supervisor/supervisord.log) should help with troubleshooting.


  [1]: http://feedertweeter.net/
  [2]: http://wearemanifold.com/if-you-build-it-they-will-feed/
  [3]: http://fritzing.org/
  [4]: http://www.raspberrypi.org/downloads
  [5]: https://github.com/shaurz/fsmonitor
  [6]: http://supervisord.org/
  [7]: https://dev.twitter.com/apps