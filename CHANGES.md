# Change Log
The following is a log of changes made since the first public release.

###v2.0
* Switched Python pin reference from GPIO.BCM to GPIO.BOARD so that pin numbers are consistent across boards and revisions.
* Moving sensor pins around to avoid conflicts with the new SleepyPi board. Note the original fritzing diagrams do not work with the latest code.  If you are wiring based on the existing fritzing diagrams then checkout the v1.6 tag rather than the latest master branch.